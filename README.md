#The API searches for a movie with given title name and logs every search request as well.

##Below Installations Required to run the project:
1. JRE 1.8
2. Maven
3. Redis at port 6379
4. Mongodb at port 27017


## Steps to run the project:
1. Clone the repository on local system. 
2. Compile the project using command: mvn clean install package -DskipTests
3. Install and setup Redis cache as per properties given in application.properties file.
4. Install and create test database in mongodb per properties defined in application.properties file.
5. Go to sample-microservice-web/target folder and run java -jar sample-microservice-web-0.0.1-SNAPSHOT.jar
6. In postman  paste below url to run the api:-
	1. Add Header service.auth.key and value 1234567 to access the api
	2. localhost:8080/test/searchMovie?title=ABCD&username=a
7. To run api from broswer
	1. Add ModHead extension to chrome and put security credentials service.auth.key and value 1234567 
	2. localhost:8080/test/searchMovie?title=ABCD&username=a

	 
	