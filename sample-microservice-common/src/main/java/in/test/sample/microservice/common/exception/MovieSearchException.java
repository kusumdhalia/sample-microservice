package in.test.sample.microservice.common.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;

public class MovieSearchException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 141009267845337473L;
    public Date timestamp;
    public HttpStatus httpStatus;
    public String errorCode;
    public String errorMessage;

    
    public MovieSearchException(Date timestamp, HttpStatus httpStatus,
            String errorMessage) {
        super();
        this.timestamp = timestamp;
        this.httpStatus = httpStatus;
        this.errorMessage = errorMessage;
    }

    public MovieSearchException(Date timestamp, HttpStatus httpStatus,
            String errorCode, String errorMessage) {
        super();
        this.timestamp = timestamp;
        this.httpStatus = httpStatus;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
