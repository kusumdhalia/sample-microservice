package in.test.sample.microservice.common.exception;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@EnableWebMvc
@RestControllerAdvice
public class MovieSearchExceptionHandler
        extends
            ResponseEntityExceptionHandler {

    @ExceptionHandler({InvocationTargetException.class})
    public final ResponseEntity<Object> handleUserNotFoundException(
            MovieSearchException ex, WebRequest request) {

        MovieSearchException errorDetails = new MovieSearchException(new Date(),
                ex.httpStatus, ex.errorCode, ex.errorMessage);
        return new ResponseEntity<Object>(errorDetails, new HttpHeaders(),
                errorDetails.httpStatus);
    }
}
