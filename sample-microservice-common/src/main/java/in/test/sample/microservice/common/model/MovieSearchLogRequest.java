package in.test.sample.microservice.common.model;

import java.io.Serializable;
import java.util.Date;

public class MovieSearchLogRequest implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2570963222371423240L;
    private Integer id;
    private String movieTitle;
    private String username;
    private Date requestedOn;

    public MovieSearchLogRequest(String movieTitle, String username,
            Date requestedOn) {
        super();
        this.movieTitle = movieTitle;
        this.setUsername(username);
        this.requestedOn = requestedOn;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getMovieTitle() {
        return movieTitle;
    }
    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getRequestedOn() {
        return requestedOn;
    }
    public void setRequestedOn(Date requestedOn) {
        this.requestedOn = requestedOn;
    }

}
