package in.test.sample.microservice.common.model;

import java.io.Serializable;
import java.util.Date;

public class MovieSearchLogResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1894031052927218263L;
    private Integer id;
    private String movieTitle;
    private Date requestedOn;

    public MovieSearchLogResponse(String movieTitle, Date requestedOn) {
        super();
        this.movieTitle = movieTitle;
        this.requestedOn = requestedOn;
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getMovieTitle() {
        return movieTitle;
    }
    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }
    public Date getRequestedOn() {
        return requestedOn;
    }
    public void setRequestedOn(Date requestedOn) {
        this.requestedOn = requestedOn;
    }

}
