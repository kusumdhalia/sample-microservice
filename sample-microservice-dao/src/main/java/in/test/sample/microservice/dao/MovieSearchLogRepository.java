package in.test.sample.microservice.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import in.test.sample.microservice.dao.model.MovieSearchLog;

/**
 * 
 * interface that allows us to do CRUD(Create/Update/Read/Delete)
 *
 */
public interface MovieSearchLogRepository
        extends
            MongoRepository<MovieSearchLog, Integer> {

}
