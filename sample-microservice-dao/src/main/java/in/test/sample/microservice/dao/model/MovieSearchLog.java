package in.test.sample.microservice.dao.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;

public class MovieSearchLog implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5959098876191968438L;
    @Id
    private String id;
    private String movieTitle;
    private String username;
    private Date requestedOn;

    public MovieSearchLog(String movieTitle, String username, Date requestedOn) {
        super();
        this.movieTitle = movieTitle;
        this.username = username;
        this.requestedOn = requestedOn;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getMovieTitle() {
        return movieTitle;
    }
    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public Date getRequestedOn() {
        return requestedOn;
    }
    public void setRequestedOn(Date requestedOn) {
        this.requestedOn = requestedOn;
    }

}
