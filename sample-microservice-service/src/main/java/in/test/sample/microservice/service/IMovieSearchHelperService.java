package in.test.sample.microservice.service;

import in.test.sample.microservice.common.exception.MovieSearchException;
import in.test.sample.microservice.common.model.MovieSearchResponse;

public interface IMovieSearchHelperService {
    
    /**
     * 
     * @param title
     * @return Response of search with given title
     * @throws MovieSearchException
     */
	public MovieSearchResponse searchMovieWithTitleFromOMDB(String title) throws MovieSearchException;
}
