package in.test.sample.microservice.service;

import in.test.sample.microservice.common.exception.MovieSearchException;
import in.test.sample.microservice.common.model.MovieSearchLogRequest;

public interface IMovieSearchLogService {

    /**
     * 
     * @param movieSearchLogRequest
     * @return true/false
     * @throws MovieSearchException
     */
    public Boolean saveMovieSearchLog(
            MovieSearchLogRequest movieSearchLogRequest)
            throws MovieSearchException;

}
