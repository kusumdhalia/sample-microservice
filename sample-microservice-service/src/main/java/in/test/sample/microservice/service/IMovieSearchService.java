package in.test.sample.microservice.service;

import in.test.sample.microservice.common.exception.MovieSearchException;
import in.test.sample.microservice.common.model.MovieSearchResponse;

public interface IMovieSearchService {

    /**
     * 
     * @param title
     * @param username
     * @return {@link MovieSearchResponse}
     * @throws MovieSearchException
     */
    public MovieSearchResponse searchMovieWithTitle(String title,
            String username) throws MovieSearchException;
}
