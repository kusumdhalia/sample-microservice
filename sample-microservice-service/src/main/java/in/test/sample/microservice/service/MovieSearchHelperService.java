package in.test.sample.microservice.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import in.test.sample.microservice.common.exception.MovieSearchException;
import in.test.sample.microservice.common.model.MovieSearchResponse;

@Service
public class MovieSearchHelperService implements IMovieSearchHelperService{
    
    @Value("${omdb.api.key}")
    String apiKey;

    @Value("${omdb.api.url}")
    String apiUrl;

    public MovieSearchResponse searchMovieWithTitleFromOMDB(String title)
            throws MovieSearchException {

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-RapidAPI-Host",
                "movie-database-imdb-alternative.p.rapidapi.com");
        headers.set("X-RapidAPI-Key", apiKey);

        HttpEntity<String> requestEntity = new HttpEntity<String>(null,
                headers);

        RestTemplate restTemplate = new RestTemplate();
        String url = apiUrl + "?apikey=" + apiKey + "&t=" + title;
        ResponseEntity<String> result = restTemplate.exchange(url,
                HttpMethod.GET, requestEntity, String.class);

        HttpStatus status = result.getStatusCode();

        if (status == HttpStatus.OK) {
            return getMovieSearchResponse(result);
        } else {
            throw new MovieSearchException(new Date(),
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Error occured while searching movie.");
        }

    }

    private MovieSearchResponse getMovieSearchResponse(
            ResponseEntity<String> result) throws MovieSearchException {

        String json = result.getBody();
        Gson gson = new Gson();
        MovieSearchResponse movieSearchResponse = gson.fromJson(json,
                MovieSearchResponse.class);

        if (("false").equalsIgnoreCase(movieSearchResponse.getResponse())) {
            throw new MovieSearchException(new Date(),
                    HttpStatus.INTERNAL_SERVER_ERROR, "Movie not found.");
        }
        return movieSearchResponse;
    }

}
