package in.test.sample.microservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.test.sample.microservice.common.exception.MovieSearchException;
import in.test.sample.microservice.common.model.MovieSearchLogRequest;
import in.test.sample.microservice.dao.MovieSearchLogRepository;
import in.test.sample.microservice.dao.model.MovieSearchLog;

@Service
public class MovieSearchLogService implements IMovieSearchLogService {

    @Autowired
    private MovieSearchLogRepository movieSearchLogRepository;

    public Boolean saveMovieSearchLog(
            MovieSearchLogRequest movieSearchLogRequest)
            throws MovieSearchException {
        MovieSearchLog movieSearchLogEntity = new MovieSearchLog(
                movieSearchLogRequest.getMovieTitle(),
                movieSearchLogRequest.getUsername(),
                movieSearchLogRequest.getRequestedOn());
        movieSearchLogEntity = movieSearchLogRepository
                .save(movieSearchLogEntity);
        if (movieSearchLogEntity.getId() != null) {
            return true;
        }
        return false;
    }

}
