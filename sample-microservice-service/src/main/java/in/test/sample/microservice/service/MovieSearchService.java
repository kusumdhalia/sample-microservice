package in.test.sample.microservice.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import in.test.sample.microservice.common.exception.MovieSearchException;
import in.test.sample.microservice.common.model.MovieSearchLogRequest;
import in.test.sample.microservice.common.model.MovieSearchResponse;

@Service
public class MovieSearchService implements IMovieSearchService {

    @Autowired
    private IMovieSearchLogService movieSearchLogService;

    @Autowired
    private IMovieSearchHelperService movieSearchHelperService;

    @Cacheable(key = "#root.methodName + #title +#username")
    public MovieSearchResponse searchMovieWithTitle(String title,
            String username) throws MovieSearchException {

        MovieSearchLogRequest movieSearchLogRequest = new MovieSearchLogRequest(
                title, username, new Date());
        Boolean response = movieSearchLogService
                .saveMovieSearchLog(movieSearchLogRequest);
        if (response) {
            return movieSearchHelperService.searchMovieWithTitleFromOMDB(title);
        } else {
            throw new MovieSearchException(new Date(),
                    HttpStatus.INTERNAL_SERVER_ERROR, title);
        }

    }

}
