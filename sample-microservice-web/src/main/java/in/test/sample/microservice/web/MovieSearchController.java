package in.test.sample.microservice.web;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.test.sample.microservice.common.exception.MovieSearchException;
import in.test.sample.microservice.common.model.MovieSearchResponse;
import in.test.sample.microservice.service.IMovieSearchService;

@RestController
@RequestMapping("/test")
public class MovieSearchController {

    @Autowired
    private IMovieSearchService service;

    @GetMapping("/searchMovie")
    public @ResponseBody MovieSearchResponse searchMovie(String title, String username)
            throws MovieSearchException {

        if (title == null || title == "" || title.trim() == "" || title.isEmpty()) {
            throw new MovieSearchException(new Date(),
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Movie title can not be empty.");

        }
        if(username == null || username == "" || username.trim() == "" || username.isEmpty()) {
            throw new MovieSearchException(new Date(),
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Username can not be empty.");
        }
        MovieSearchResponse movieSearchResponse = service
                .searchMovieWithTitle(title,username);
        return movieSearchResponse;
    }

}
