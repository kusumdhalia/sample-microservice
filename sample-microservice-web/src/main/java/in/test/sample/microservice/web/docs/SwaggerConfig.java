package in.test.sample.microservice.web.docs;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Value("${service.auth.key}")
	private String authKey;
	
	@Value("${service.auth.header.name}")
	private String authHeaderName;

	@Bean
	public Docket apiDocket() {
		List<ApiKey> apiKeys = new ArrayList<ApiKey>();
		ApiKey key = new ApiKey(authKey, authHeaderName, "header");
		apiKeys.add(key);
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select().paths(regex("/api.*")).build()
				.securitySchemes(apiKeys);
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Sample API")
				.description("A sample implementation of an API documentation using Swagger.").contact("Shark Team")
				.version("1.0").build();
	}
}