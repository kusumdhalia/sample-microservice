package in.test.sample.microservice;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import in.test.sample.microservice.app.SampleApplication;
import in.test.sample.microservice.common.model.MovieSearchResponse;
import in.test.sample.microservice.service.IMovieSearchService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SampleApplication.class)
public class TestService {

    @Autowired
    private IMovieSearchService movieSearchService;

    @Test
    public void testMovieSearch1() {
        MovieSearchResponse res1 = movieSearchService
                .searchMovieWithTitle("titanic","kusum@gmail.com");
        assertTrue(res1 != null);
    }
    
    @Test
    public void testMovieSearch2() {
        MovieSearchResponse res1 = movieSearchService
                .searchMovieWithTitle("","kusum@gmail.com");
        assertTrue(res1 != null);
    }
    
    @Test
    public void testMovieSearch3() {
        MovieSearchResponse res1 = movieSearchService
                .searchMovieWithTitle("titanic","");
        assertTrue(res1 != null);
    }
    
    @Test
    public void testMovieSearch4() {
        MovieSearchResponse res1 = movieSearchService
                .searchMovieWithTitle("","");
        assertTrue(res1 != null);
    }

}
